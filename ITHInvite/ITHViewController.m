//
//  ViewController.m
//  ITHInvite
//
//  Created by Ilya on 15/2/17.
//  Copyright © 2017 Ilya. All rights reserved.
//

#import "ITHViewController.h"
#import "ITHFetch.h"

@interface ITHViewController () {
    NSArray *array;
}


@end

@implementation ITHViewController

- (void)viewDidLoad {
    self.date.maximumDate = [NSDate date];
    self.valCode.text = nil;
    [super viewDidLoad];
}

- (IBAction)search:(id)sender {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyyMMdd"];
    array = [ITHFetch fetchData:[formatter stringFromDate:[self.date date]] andCode:self.valCode.text];
    if([array count] == 1) {
        self.outputText.text = [NSString stringWithFormat:@"%@\t\t%@",
                                [[array objectAtIndex:0] objectForKey:@"cc"],
                                [[array objectAtIndex:0] objectForKey:@"rate"]];
    } else {
        int i = 0;
        while (i != [array count]) {
            NSString *cache = self.outputText.text;
            self.outputText.text = [NSString stringWithFormat:@"%@%@\t\t%@\n", cache,
                                    [[array objectAtIndex:i] objectForKey:@"cc"],
                                    [[array objectAtIndex:i] objectForKey:@"rate"]];
            i++;
        }
    }
}
@end
