//
//  ITHFetch.h
//  ITHInvite
//
//  Created by Ilya on 15/2/17.
//  Copyright © 2017 Ilya. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ITHFetch : NSObject

+ (NSArray *) fetchData:(NSString *)date andCode:(NSString *)code;

@end
