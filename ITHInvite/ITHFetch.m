//
//  ITHFetch.m
//  ITHInvite
//
//  Created by Ilya on 15/2/17.
//  Copyright © 2017 Ilya. All rights reserved.
//

#import "ITHFetch.h"

@implementation ITHFetch

+ (NSArray *) fetchData:(NSString *)date andCode:(NSString *)code {
    if ([code isEqualToString:@""]) {
        NSString *urlString = [NSString stringWithFormat:@"https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?date=%@&json", date];
        NSError *err;
        NSString *json = [[NSString alloc] initWithContentsOfURL:[NSURL URLWithString:urlString]
                                                        encoding:NSUTF8StringEncoding
                                                           error:&err];
        NSData *data = [json dataUsingEncoding:NSUTF8StringEncoding];
        id array = [NSJSONSerialization JSONObjectWithData:data
                                                         options:kNilOptions
                                                           error:&err];
        return array;
    } else {
        NSString *urlString = [NSString stringWithFormat:@"https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?valcode=%@&date=%@&json", code, date];
        NSError *err;
        NSString *json = [[NSString alloc] initWithContentsOfURL:[NSURL URLWithString:urlString]
                                                        encoding:NSUTF8StringEncoding
                                                           error:&err];
        
        NSData *data = [json dataUsingEncoding:NSUTF8StringEncoding];
        id array = [NSJSONSerialization JSONObjectWithData:data
                                                         options:kNilOptions
                                                           error:&err];
        return array;
    }
}

@end
