//
//  ViewController.h
//  ITHInvite
//
//  Created by Ilya on 15/2/17.
//  Copyright © 2017 Ilya. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ITHViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIDatePicker *date;
@property (weak, nonatomic) IBOutlet UITextField *valCode;
@property (weak, nonatomic) IBOutlet UITextView *outputText;
- (IBAction)search:(id)sender;

@end

